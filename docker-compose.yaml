version: "3.3"
services:
  reverse-proxy:
    image: traefik:v2.11
    command:
      - --log.level=WARN
      # insecure
      #- --api.insecure=true
      - --providers.docker
      - --entryPoints.web.address=:80
      - --entrypoints.web.http.redirections.entryPoint.to=websecure
      - --entrypoints.web.http.redirections.entryPoint.scheme=https
      - --entryPoints.websecure.address=:443
      - --entryPoints.web.forwardedHeaders.trustedIPs=172.16.0.0/12,192.168.0.0/16
      - --entryPoints.web.transport.respondingTimeouts.idleTimeout=600
      - --certificatesresolvers.myresolver.acme.email=tom@tcweb.org
      - --certificatesresolvers.myresolver.acme.storage=/acme.json
      # used during the challenge
      - --certificatesresolvers.myresolver.acme.httpchallenge.entrypoint=web
    ports:
      - "80:80"
      - "443:443"
      # The Web UI (enabled by --api.insecure=true)
      #- "8080:8080"
    depends_on:
      - nextcloud-front
      - collabora
    volumes:
      - /var/run/docker.sock:/var/run/docker.sock
      - ./acme.json:/acme.json
    restart: unless-stopped
    labels:
      traefik.frontend.passHostHeader: "true"
    networks:
      nextcloud:
        aliases:
          - traefik

  nextcloud-front:
    image: nginx:latest
    depends_on:
      - nextcloud-app
    restart: unless-stopped
    volumes:
      - ./nextcloud-data/:/var/www/html:ro
      - ./nginx.conf:/etc/nginx/conf.d/nextcloud.conf:ro
    labels:
      - "traefik.http.routers.front.rule=Host(`cloud.${DOMAIN}`)"
      - "traefik.http.routers.front.entryPoints=web,traefik"
      - "traefik.http.services.front.loadbalancer.server.port=80"
      - "traefik.http.routers.front-ssl.rule=Host(`cloud.${DOMAIN}`)"
      - "traefik.http.routers.front-ssl.entryPoints=websecure"
      - "traefik.http.routers.front-ssl.tls=true"
      - "traefik.http.routers.front-ssl.service=front"
      - "traefik.http.routers.front-ssl.tls.certresolver=myresolver"
      - "traefik.http.routers.front-ssl.priority=1"
      - "traefik.http.routers.front.middlewares=nextcloud_redirectregex"
      - "traefik.http.middlewares.nextcloud_redirectregex.redirectregex.permanent=true"
      - "traefik.http.middlewares.nextcloud_redirectregex.redirectregex.regex=https://(.*)/.well-known/(?:card|cal)dav"
      - "traefik.http.middlewares.nextcloud_redirectregex.redirectregex.replacement=https://$${1}/remote.php/dav"
    networks:
      nextcloud:
        aliases:
          - front

  mail-front:
    image: docker.io/traefik/whoami:latest
    labels:
      - "traefik.http.routers.mail.rule=Host(`mail.${DOMAIN}`)"
      - "traefik.http.routers.mail.entryPoints=web,traefik"
      - "traefik.http.services.mail.loadbalancer.server.port=80"
      - "traefik.http.routers.mail-ssl.rule=Host(`mail.${DOMAIN}`)"
      - "traefik.http.routers.mail-ssl.entryPoints=websecure"
      - "traefik.http.routers.mail-ssl.tls=true"
      - "traefik.http.routers.mail-ssl.service=mail"
      - "traefik.http.routers.mail-ssl.tls.certresolver=myresolver"
    networks:
      nextcloud:
        aliases:
          - whoami

  collabora:
    image: collabora/code
    restart: unless-stopped
    labels:
      - "traefik.http.routers.collabora.rule=Host(`office.${DOMAIN}`)"
      - "traefik.http.routers.collabora.entryPoints=web,traefik"
      - "traefik.http.services.collabora.loadbalancer.server.port=9980"
      - "traefik.http.routers.collabora-ssl.rule=Host(`office.${DOMAIN}`)"
      - "traefik.http.routers.collabora-ssl.entryPoints=websecure"
      - "traefik.http.routers.collabora-ssl.tls=true"
      - "traefik.http.routers.collabora-ssl.service=collabora"
      - "traefik.http.routers.collabora-ssl.tls.certresolver=myresolver"
    environment:
      extra_params: "--o:ssl.enable=false --o:ssl.termination=true --o:net.post_allow.host[0]=.+ --o:storage.wopi.host[0]=.+"
      domain: "cloud.${DOMAIN}"
    cap_add:
      - MKNOD
    networks:
      nextcloud:
        aliases:
          - collabora

  nextcloud-app:
    image: nextcloud:30-fpm
    environment:
      MYSQL_USER: nextcloud
      MYSQL_PASSWORD: "${MYSQL_PASSWORD}"
      MYSQL_DATABASE: nextcloud
      MYSQL_HOST: mysql
      REDIS_HOST: redis
      PHP_MEMORY_LIMIT: 4096M
      PHP_UPLOAD_LIMIT: 16G
      NEXTCLOUD_TRUSTED_DOMAINS: "cloud.${DOMAIN}"
    depends_on:
      - nextcloud-db
      - nextcloud-redis
    restart: unless-stopped
    volumes:
      - ./nextcloud-data/:/var/www/html
    networks:
      nextcloud:
        aliases:
          - app

  imaginary:
    image: nextcloud/aio-imaginary
    restart: unless-stopped
    cap_add:
     - SYS_NICE
    networks:
      nextcloud:
        aliases:
          - imaginary

  # https://help.nextcloud.com/t/setting-up-files-high-performance-backend/108651/10
  nextcloud-push:
    image: nextcloud:30-fpm
    environment:
      PORT: 7867
      NEXTCLOUD_URL: http://front/
    entrypoint: /var/www/html/custom_apps/notify_push/bin/x86_64/notify_push /var/www/html/config/config.php
    depends_on:
      - nextcloud-db
      - nextcloud-redis
      - nextcloud-front
    restart: unless-stopped
    volumes:
      - ./nextcloud-data/:/var/www/html
    labels:
      - traefik.enable=true
      - traefik.protocol=http
      - traefik.port=7867
      - traefik.docker.network=nextcloud
      - traefik.http.services.nextcloud-push.loadbalancer.server.port=7867
      - traefik.http.routers.nextcloud-push.priority=2
      - traefik.http.routers.nextcloud-push.middlewares=nextcloud_strip_push
      - traefik.http.routers.nextcloud-push.tls=true
      - traefik.http.routers.nextcloud-push.entryPoints=websecure
      - traefik.http.routers.nextcloud-push.tls.certresolver=myresolver
      - traefik.http.routers.nextcloud-push.rule=Host(`cloud.${DOMAIN}`)
      #- traefik.http.routers.nextcloud-push.service=push
      # necessary for the notify_push app to work:
      - traefik.http.routers.nextcloud-push.rule=Host(`cloud.${DOMAIN}`) && PathPrefix(`/push`)
      - traefik.http.middlewares.nextcloud_strip_push.stripprefix.prefixes=/push
    networks:
      nextcloud:
        aliases:
          - push

  coturn:
    image: registry.gitlab.com/azae/docker/coturn
    restart: unless-stopped
    ports:
      - "49152-49200:49152-49200/udp"
      - "3478:3478"
      - "3478:3478/udp"
      - "3479:3479"
      - "3479:3479/udp"
    environment:
      DOMAIN: "turn.${DOMAIN}"
      SECRET: "${COTURN_PASSWORD}"
    networks:
      coturn:
        aliases:
          - coturn

  nextcloud-cron:
    image: nextcloud:30-fpm
    environment:
      MYSQL_USER: nextcloud
      MYSQL_PASSWORD: "${MYSQL_PASSWORD}"
      MYSQL_DATABASE: nextcloud
      MYSQL_HOST: mysql
      REDIS_HOST: redis
      PHP_MEMORY_LIMIT: 8192M
    entrypoint: /cron.sh
    depends_on:
      - nextcloud-db
      - nextcloud-redis
    restart: unless-stopped
    volumes:
      - ./nextcloud-data/:/var/www/html
    networks:
      nextcloud:
        aliases:
          - cron

  nextcloud-redis:
    image: redis:latest
    restart: unless-stopped
    networks:
      nextcloud:
        aliases:
          - redis

  nextcloud-db:
    image: mariadb:latest
    restart: unless-stopped
    environment:
      MYSQL_ROOT_PASSWORD: "${MYSQL_ROOT_PASSWORD}"
      MYSQL_PASSWORD: "${MYSQL_PASSWORD}"
      MYSQL_DATABASE: nextcloud
      MYSQL_USER: nextcloud
    command: --innodb_read_only_compressed=OFF
    volumes:
      - ./nextcloud-db:/var/lib/mysql
      - ./nextcloud-conf/mariadb:/etc/mysql/conf.d 
    networks:
      nextcloud:
        aliases:
          - mysql

  pleroma-db:
    image: postgres:15
    restart: always
    environment:
      POSTGRES_USER: pleroma
      POSTGRES_PASSWORD: $PLEROMA_DB_PASSWORD
      POSTGRES_DB: pleroma
    volumes:
      - ./pleroma/postgres-15:/var/lib/postgresql/data
      - ./pleroma/conf/postgresql.conf:/etc/postgresql/postgresql.conf
    networks:
      pleroma:
        aliases:
          - db

  pleroma-web:
    image: git.pleroma.social:5050/pleroma/pleroma:latest
    restart: always
    volumes:
      - ./pleroma/uploads:/var/lib/pleroma/uploads
      - ./pleroma/static:/var/lib/pleroma/static
      #- ./pleroma/conf:/etc/pleroma
      - ./pleroma/config.exs:/etc/pleroma/config.exs
      # optional, see 'Config Override' section in README.md
      - ./pleroma/conf/config.exs:/var/lib/pleroma/config.exs:ro
    environment:
      DOMAIN: pleroma.tcweb.org
      INSTANCE_NAME: TcWeb Pleroma
      ADMIN_EMAIL: tom@tcweb.org
      NOTIFY_EMAIL: tom@tcweb.org
      DB_USER: pleroma
      DB_PASS: $PLEROMA_DB_PASSWORD
      DB_NAME: pleroma
      DB_HOST: db
    depends_on:
      - pleroma-db
    labels:
      - "traefik.http.routers.pleroma.rule=Host(`pleroma.${DOMAIN}`)"
      - "traefik.http.routers.pleroma.entryPoints=web,traefik"
      - "traefik.http.services.pleroma.loadbalancer.server.port=4000"
      - "traefik.http.routers.pleroma-ssl.rule=Host(`pleroma.${DOMAIN}`)"
      - "traefik.http.routers.pleroma-ssl.entryPoints=websecure"
      - "traefik.http.routers.pleroma-ssl.tls=true"
      - "traefik.http.routers.pleroma-ssl.service=pleroma"
      - "traefik.http.routers.pleroma-ssl.tls.certresolver=myresolver"
    networks:
      pleroma:
        aliases:
          - web
      nextcloud:
        aliases:
          - pleroma

  mumble-server:
    image: mumblevoip/mumble-server
    container_name: mumble-server
    hostname: mumble-server
    restart: on-failure
    ports:
      - 64738:64738
      - 64738:64738/udp
    environment:
      MUMBLE_SUPERUSER_PASSWORD: "${MUMBLE_SUPERUSER_PASSWORD}"

  openvpn:
    cap_add:
     - NET_ADMIN
    image: kylemanna/openvpn
    container_name: openvpn
    ports:
     - "1194:1194/udp"
     - "1194:1194"
    restart: always
    volumes:
     - ./openvpn-data/conf:/etc/openvpn

# Custom network so all services can communicate using a FQDN
networks:
  nextcloud:
  coturn:
  pleroma:
