#!/bin/bash
wget https://raw.githubusercontent.com/nextcloud/documentation/master/admin_manual/installation/nginx-root.conf.sample -O nginx.conf
sed -e 's/server 127.0.0.1:9000;/server app:9000;/g' \
    -e '13,24d' \
    -e 's/server_name cloud.example.com;/server_name cloud.tcweb.org;\n    server_name front;/g' \
    -e 's/^    ssl/    #ssl/' \
    -e 's/ssl http2//' \
    -e 's/# fastcgi_param HTTPS/fastcgi_param HTTPS/' \
    -e 's/#add_header Strict-Transport-Security/add_header Strict-Transport-Security/' \
    -e 's/listen 443/listen 80/' \
    -e 's?/var/www/nextcloud?/var/www/html?' \
    -e 's/listen \[::\]:443/listen [::]:80/' \
    -i nginx.conf
