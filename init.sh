#!/bin/bash

function generatePassword() {
    openssl rand -hex 16
}

MYSQL_PASSWORD=$(generatePassword)
MYSQL_ROOT_PASSWORD=$(generatePassword)
COTURN_PASSWORD=$(generatePassword)
KRESUS_DB_PASSWORD=$(generatePassword)

cat "$(dirname "$0")/env.sample" | sed \
    -e "s#MYSQL_PASSWORD=.*#MYSQL_PASSWORD=${MYSQL_PASSWORD}#g" \
    -e "s#MYSQL_ROOT_PASSWORD=.*#MYSQL_ROOT_PASSWORD=${MYSQL_ROOT_PASSWORD}#g" \
    -e "s#COTURN_PASSWORD=.*#COTURN_PASSWORD=${COTURN_PASSWORD}#g" \
    -e "s#KRESUS_DB_PASSWORD=.*#KRESUS_DB_PASSWORD=${KRESUS_DB_PASSWORD}#g" \
    "$(dirname "$0")/.env"

touch "$(dirname "$0")/acme.json"
chmod 600 "$(dirname "$0")/acme.json"
