import Config

config :logger,
  level: :info

config :pleroma, :instance,
  registrations_open: false,
  federating: true

config :pleroma, Pleroma.Web.Endpoint,
  url: [host: "pleroma.tcweb.org", port: 443, scheme: "https"],
  http: [
    port: 4000,
    ip: {0, 0, 0, 0}
  ]

